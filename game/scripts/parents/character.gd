extends KinematicBody2D

export (float) var speed = 200.0
export (float) var friction = 0.01
export (float) var acceleration = 0.1
export (int) var points = 0
export (bool) var controllable = true
export (int) var health = 100
export (int) var attack = 20
export (bool) var alive = true
export (Vector2) var direction = Vector2.RIGHT

enum STATES {
	IDLE,
	MOVE,
	DETECT,
	ATTACK,
	NEW_DIRECTION
}

func _ready():
	pass # Replace with function body.
