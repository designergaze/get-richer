# Get Richer - (cancelled) Ludum Dare 47 game

Get Richer is a (cancelled) Ludum Dare 47 entry.

Most of the assets are public domain.

Except:

- Princess Quest by Abstration, from [Three Red Hearts](https://abstractionmusic.bandcamp.com/album/three-red-hearts) (CC BY-NC 3.0)

- Dino Characters by Arks (twitter.com/ScissorMarks) (CC BY 4.0)
